package com.sda.trestian.radiomusicdatabase.exception;

public class InvalidSongException extends Exception{

	public InvalidSongException() {
		super();
	}
	
	public InvalidSongException(String message) {
		super(message);
	}
}
