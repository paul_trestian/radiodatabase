package com.sda.trestian.radiomusicdatabase.exception;

public class InvalidSongLengthException extends InvalidSongException{

	public InvalidSongLengthException() {
		super();
	}
	
	public InvalidSongLengthException(String message) {
		super(message);
	}
}
