package com.sda.trestian.radiomusicdatabase.exception;

public class InvalidSongNameException extends InvalidSongException{

	public InvalidSongNameException() {
		super();
	}
	
	public InvalidSongNameException(String message) {
		super(message);
	}
}
